# Overview

These instructions explain how to configure your Raspberry Pi 2 for use with DroidHUD.  You
need:

 * Raspberry Pi 2
 * Raspberry Pi camera
 * WiFi adapter that supports AP mode.  For example, the Alfa AWUS052NH.
 * A good BEC connected directly to the GPIO header of the Raspberry Pi that can supply
   enough current for the Pi and your wireless adapter.  Pins 4 & 6 work.
 * You may also need a large (>1000uF) cap connected near the Pi GPIO header across +5V and Ground.

# Install Raspbian-lite

Jessie was used.  Follow the instructions here:
 * https://www.raspberrypi.org/downloads/raspbian/
 * https://downloads.raspberrypi.org/raspbian_lite_latest

## Run raspi-config
 * Update raspi-config
 * Expand filesystem (if needed)
 * Set password
 * Enable camera
 * Set internationalization options as appropriate
 * Set dont'-wait-for-network for boot

Add the following line to the end of `/boot/config.txt`:
```
max_usb_current=1
```
This will ensure that the wirless adapter can get enough current.

Remove/disable unnecessary packages and update the system:
```
sudo dpkg --purge dhcpcd5 dphys-swapfile avahi-daemon
apt-get update && apt-get upgrade
systemctl disable triggerhappy
```

# Install FPV software

## Install dependencies and build
```
apt-get install git autoconf automake libtool pkg-config libgstreamer1.0-dev  libgstreamer-plugins-base1.0-dev libraspberrypi-dev gstreamer-1.0 gstreamer1.0-plugins-base gstreamer1.0-tools python3 python3-rpi.gpio

cd
mkdir src
cd src
git clone https://gitlab.com/droidhud/telewii.git
git clone https://github.com/thaytan/gst-rpicamsrc.git
cd gst-rpicamsrc
./autogen.sh --prefix=/usr --libdir=/usr/lib/arm-linux-gnueabihf/
make
sudo make install
cd ../telewii
make
```

## Configure service to start at boot
Create the file `/etc/systemd/system/fpvman.service`:

```
[Unit]
Description=FPV manager for DroidHUD

[Service]
Type=simple
ExecStart=/home/pi/src/telewii/scripts/fpvman.py
KillMode=process
KillSignal=SIGINT
Restart=on-failure
User=pi

[Install]
WantedBy=multi-user.target
```

Enable the service:

```systemctl enable fpvman```

# Set up client networking for development

Contents of `/etc/network/interfaces`. Set ssid and key as needed for your local network.

```
# interfaces(5) file used by ifup(8) and ifdown(8)

# Please note that this file is written to be used with dhcpcd
# For static IP, consult /etc/dhcpcd.conf and 'man dhcpcd.conf'

# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

allow-hotplug eth0
iface eth0 inet dhcp
#iface eth0 inet manual

#allow-hotplug wlan0
iface wlan0 inet dhcp
    essid <ssid>
    wpa-ssid <ssid>
    wpa-psk <key hash>
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```

# Set up wireless access point

```
sudo apt-get install crda wireless-regdb dnsmasq hostapd
sudo vim /etc/default/crda
REGDOMAIN=<your country code, e.g. US>
```

Don't automatically start dnsmasq:

```sudo systemctl disable dnsmasq```

Create `/etc/hostpad/hostapd.conf` symlink:

```sudo ln -s /run/hostapd.conf /etc/hostapd/hostapd.conf```

Create `/etc/hostapd/hostapd.template.conf`.  The channel will be set at runtime based on jumper settings.  The assumption is that you want to use 5GHz.
```
interface=wlan0
#bridge=br0
driver=nl80211
country_code=<REGDOMAIN>
ssid=<SSID>
hw_mode=a
ieee80211n=1
ieee80211d=1
wmm_enabled=1
wpa=2
wpa_passphrase=<PASSPHRASE>
wpa_key_mgmt=WPA-PSK
wpa_pairwise=CCMP
rsn_pairwise=CCMP
macaddr_acl=0
#ht_capab=[HT40][SHORT-GI-40][RX-STBC1]
# 5745 MHz
#channel=149
# 5240 MHz
#channel=48
```

Edit `/etc/default/hostapd` and set:

```
DAEMON_CONF="/etc/hostapd/hostapd.conf"
```

Edit `/etc/dnsmasq.conf` and set:
```
interface=wlan0
dhcp-range=192.168.137.100,192.168.137.150,12h
dhcp-option=19,0
```

Edit `/etc/rc.local` and add:

```
/home/pi/src/telewii/script/start_networking.sh
```

# Cleanup
```
sudo apt-get autoremove
sudo apt-get clean
```

# Set up filesystems as read-only
If `/etc/resolv.conf` is not already a symlink:
```
cd /etc
sudo ln -s /run/resolvconf/resolv.conf .
```

Set up unionfs:
```
sudo apt-get install unionfs-fuse
ln -s /home/pi/src/telewii/scripts/mount_unionfs /usr/local/sbin/mount_unionfs
mv /var /var.orig
mkdir /var.rw
cp /etc/fstab /etc/fstab.orig
```

Update `/etc/fstab` as follows:
```
proc            /proc           proc    defaults    0       0
/dev/mmcblk0p1  /boot           vfat    ro          0       2
/dev/mmcblk0p2  /               ext4    ro,noatime  0       1
mount_unionfs   /var            fuse    defaults    0       0
none            /tmp            tmpfs   defaults    0       0
```

# Usage

If you need to change anything on the filesystem, place a jumper across board pins 13 & 14 and reboot.  Be sure to `sudo halt` when finished.  This will also cause the Raspbery Pi to set the wireless adapter to client mode and connect to your local network.

For flying, remove the jumper.  This will cause the SD card partitions to be mounted read-only and will set the wireless adapter to access point mode.

