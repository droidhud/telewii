# TeleWii

Telewii connects to Cleanflight via the MultiWii protocol and generates UDP telemetry frames for use with DroidHUD.

Also contained in this repository:
 * fpvman - a daemon to manage connections from DroidHUD and start/stop Telewii and Gstreamer
 * Scripts and to help set up a Raspbian install for use with DroidHUD

Refer to INSTALLATION.md for instructions.
