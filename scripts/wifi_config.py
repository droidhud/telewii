#!/usr/bin/env python3
import sys
import RPi.GPIO as GPIO
from subprocess import call

#CHAN_BIT0 = 27
CHAN_BIT0 = 3
CHAN_BIT1 = 10
USB_CURRENT_MODE = 38
WIFI_CHANNELS = (149,153,161,165)
TEMPLATE_FILE=sys.argv[1]
OUTPUT_FILE=sys.argv[2]

GPIO.setmode(GPIO.BCM)
GPIO.setup(CHAN_BIT0, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(CHAN_BIT1, GPIO.IN, GPIO.PUD_UP)
# Set in config.txt for now
#GPIO.setup(USB_CURRENT_MODE, GPIO.OUT, GPIO.PUD_OFF, GPIO.HIGH)

bit0 = GPIO.input(CHAN_BIT0)
bit1 = GPIO.input(CHAN_BIT1)

chan_index = 2*bit1 + bit0

wifi_channel = WIFI_CHANNELS[chan_index]
#wifi_channel = 153

print("Using wifi channel {0}".format(wifi_channel))

with open(TEMPLATE_FILE, 'r') as templatefile:
    templatelines = templatefile.readlines()

with open(OUTPUT_FILE, 'w') as outputfile:
    for line in templatelines:
        outputfile.write(line)

    outputfile.write('channel={0}\n'.format(wifi_channel))

print("hostapd config file {0} generated".format(OUTPUT_FILE))
        
