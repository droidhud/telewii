#!/usr/bin/env python
import RPi.GPIO as GPIO

#MODE_GPIO = 5
MODE_GPIO = 13
DEV_MODE_DEV = 0
DEV_MODE_RUN = 1

GPIO.setmode(GPIO.BOARD) 
GPIO.setup(MODE_GPIO, GPIO.IN, GPIO.PUD_UP)

wifi_mode = GPIO.input(MODE_GPIO)

if wifi_mode == DEV_MODE_DEV:
    print("DEV")
else:
    print("RUN")

