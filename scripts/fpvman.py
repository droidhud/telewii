#!/usr/bin/env python3
import socket
import struct
import subprocess
import hashlib
import sys

IP = "0.0.0.0"
LISTEN_PORT = 1236
VIDEO_CMD_STRING = "gst-launch-1.0 rpicamsrc hflip=true vflip=true bitrate={5} keyframe-interval={2} ! video/x-h264,width={3},height={4},framerate={2}/1,profile=high ! rtph264pay config-interval=1 ! udpsink host={0} port={1}"

TELEM_DEV = '/dev/ttyUSB0'
TELEM_BIN = '/home/pi/src/telewii/telewii'

MAGIC = 0xc0c0f00d 
MSG_TYPE_CONNECT = 0x0000000C
MSG_TYPE_DISCO = 0x000000DC

current_client = None

def logmsg(*objs):
    print(*objs, file=sys.stdout)
    sys.stdout.flush()

class Client():
    """
    Represents a client connection
    """

    def __init__(self, address, tport, tfreq, tdiv, vport, vfps, vwidth, vheight, vbitrate, hashval):
        self.address = address
        self.vport = vport
        self.tport = tport
        self.tfreq = tfreq
        self.tdiv = tdiv
        self.vfps = vfps
        self.vwidth = vwidth
        self.vheight = vheight
        self.vbitrate = vbitrate
        self.hashval = hashval
        self.video_process = None
        self.telem_process = None

    def same_client(self, hashval):
        return hashval == self.hashval

    def start_telemetry_stream(self):
        cmd = [TELEM_BIN, '-s', TELEM_DEV, '-f', str(self.tfreq), '-o', str(self.tdiv)]
        cmd.append('-d')
        cmd.append(self.address)
        cmd.append('-p')
        cmd.append(str(self.tport))

        self.telem_process = subprocess.Popen(cmd)

    def start_video_stream(self):
        cmd = VIDEO_CMD_STRING.format(self.address, str(self.vport), 
                str(self.vfps), str(self.vwidth), str(self.vheight), str(self.vbitrate))
        #print(cmd.split(' '))

        self.video_process = subprocess.Popen(cmd.split(' '))

    def stop_streams(self):
        logmsg("Killing streams...")
        for process in [self.video_process, self.telem_process]:
            if process:
                process.terminate()

        self.video_process = None
        self.telem_process = None

def genhashval(data):
    # Hash the config for easy comparison
    h = hashlib.md5()
    h.update(data)

    return h.hexdigest()

def parse_packet(data, addr):
    msg = {}
    if len(data) < 12:
        return False, msg

    decoded_packet = struct.unpack('!IIhh', data[0:12])
    pkt_magic = decoded_packet[0]
    msg['type'] = decoded_packet[1]
    msg['video_port'] = decoded_packet[2]
    msg['telem_port'] = decoded_packet[3]

    if pkt_magic != MAGIC:
        logmsg("Magic BAD")
        return False, msg
    else:
        #print("Magic GOOD")

        if msg['type'] == MSG_TYPE_CONNECT:
            decoded_packet = struct.unpack('!hhhIhh', data[12:26])
            msg['video_width'] = decoded_packet[0]
            msg['video_height'] = decoded_packet[1]
            msg['video_fps'] = decoded_packet[2]
            msg['video_bitrate'] = decoded_packet[3]
            msg['telem_att_freq'] = decoded_packet[4]
            msg['telem_div'] = decoded_packet[5]

        return True, msg

def main():
    global current_client

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    sock.bind((IP,LISTEN_PORT))

    logmsg("Ready...")

    while True:
        data, addr = sock.recvfrom(1024)
        good_packet, msg = parse_packet(data, addr)
        hashval = genhashval(data)

        if good_packet and msg['type'] == MSG_TYPE_CONNECT:
            # Is this a new address?
            if current_client:
                if current_client.same_client(hashval):
                    new_client = False
                else:
                    new_client = True
            else:
                new_client = True

            if new_client:
                logmsg('New client at {0}. Video:{1} Telemetry:{2}'.format( \
                        addr, msg['video_port'], msg['telem_port']))
                # Kill any current streams
                if current_client:
                    current_client.stop_streams()

                # Start new streams
                current_client = Client(addr[0], 
                        msg['telem_port'], msg['telem_att_freq'], msg['telem_div'], \
                        msg['video_port'], msg['video_fps'], \
                        msg['video_width'], msg['video_height'], \
                        msg['video_bitrate'], hashval)

                current_client.start_telemetry_stream()
                current_client.start_video_stream()

        elif good_packet and msg['type'] == MSG_TYPE_DISCO:
            logmsg('Disconnect received from {0}'.format(addr))
            # Kill any current streams
            if current_client:
                current_client.stop_streams()
            current_client = None


if __name__ == '__main__':
    try:
        main()
    except (KeyboardInterrupt, SystemExit):
        logmsg("Caught interrupt, cleaning up")
        if current_client:
            current_client.stop_streams()
        logmsg("Exiting...")

