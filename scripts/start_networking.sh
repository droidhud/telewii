#!/bin/sh

WIRED_IFACE=eth0
WIFI_IFACE=wlan0
WIFI_IP=192.168.137.1
DEV_MODE_TOOL=/home/pi/src/telewii/scripts/dev_mode.py
WIFI_CONFIG_TOOL=/home/pi/src/telewii/scripts/wifi_config.py
WIFI_CONFIG_TMPL=/etc/hostapd/hostapd.template.conf
WIFI_CONFIG_OUT=/run/hostapd.conf

# Get mode: RUN or DEV
DEV_MODE=`${DEV_MODE_TOOL}`

# Shut off wired interface if we came up in RUN mode and nothing is
# connected
# TODO maybe fix this later
#WIRED_CARRIER_FILE=/sys/class/net/${WIRED_IFACE}/carrier
#WIRED_CONNECTED=`cat ${WIRED_CARRIER_FILE}`
#WIRED_CONNECTED_ERR=$?
#if [ "${DEV_MODE}" = "RUN" ]; then
#    if [ "${WIRED_CONNECTED_ERR}" -eq 0 -a "${WIRED_CONNECTED}" -eq 0 ]; then
#        echo Physical link not detected, disabling ${WIRED_IFACE}...
#        ifdown ${WIRED_IFACE}
#        ifconfig ${WIRED_IFACE} down
#    fi
#fi

if [ "${DEV_MODE}" = "RUN" ]; then
    ifdown ${WIRED_IFACE}
    ifconfig ${WIRED_IFACE} down
fi

# Start wireless in appropriate mode
if [ -d /sys/class/net/${WIFI_IFACE} ]; then
    #/etc/init.d/dnsmasq stop
    #/etc/init.d/hostapd stop
    systemctl stop dnsmasq
    systemctl stop hostapd
    ifdown $WIFI_IFACE
    #ifconfig $WIFI_IFACE down


    if [ "${DEV_MODE}" = "RUN" ]; then 
        echo Starting wifi in AP mode...
        # Reduce TX power to reduce chance of brownout
        #iw dev wlan0 set txpower fixed 2000
        iwconfig $WIFI_IFACE txpower 24
        $WIFI_CONFIG_TOOL $WIFI_CONFIG_TMPL $WIFI_CONFIG_OUT
        #/etc/init.d/hostapd start
        systemctl start hostapd
        ifconfig $WIFI_IFACE $WIFI_IP
        #/etc/init.d/dnsmasq start
        systemctl start dnsmasq
    else
        echo Starting wifi in Client mode...
        # Reduce TX power so we can use an AC adapter?
        iwconfig $WIFI_IFACE power on
        iwconfig $WIFI_IFACE txpower 17
        ifup $WIFI_IFACE
    fi
else
    echo Wifi interface ${WIFI_IFACE} not present...
fi

exit 0
