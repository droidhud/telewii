#ifndef _TELEWII_H
#define _TELEWII_H
/*
** Copyright 2015 by Joel Fuster <j@fuster.org>
** 
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License version 2, as 
** published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
** 02110-1301  USA
*/

#define TELEM_SYNC_WORD     0xF005B411UL


typedef union address
{
    struct sockaddr sa;
    struct sockaddr_in sa_in;
    struct sockaddr_in6 sa_in6;
    struct sockaddr_storage sa_storage;
} address_t;

typedef struct
{
    uint32_t    sync;
    uint32_t    crc;        /* Covers seqnum to end of data */
    uint32_t    seqnum;
    uint16_t    type;
    uint16_t    data_len;
    uint8_t     data[];
} telem_header_t;

typedef enum
{
    TELEM_MSG_MSP,
    TELEM_MSG_STATS,
    TELEM_MSG_FRAME
} TELEM_MSG_TYPE;

typedef enum {
    MSP_RX_STATE_SYNC1,
    MSP_RX_STATE_SYNC2,
    MSP_RX_STATE_SYNC3,
    MSP_RX_STATE_LEN,
    MSP_RX_STATE_MSG_TYPE,
    MSP_RX_STATE_DATA,
    MSP_RX_STATE_CHKSUM
} MSP_RX_STATE_TYPE;


typedef struct {
    uint32_t            mspSyncErrors;
    uint32_t            mspBadChecksums;
    MSP_RX_STATE_TYPE   mspRxState;
    uint8_t             dataLen;
    uint8_t             checksum;
    uint8_t             dataBytes;
    bool                mspMsgGood;
} msp_parse_state_t;

int open_socket(const char *hostname, const char *port, bool force_ipv4);
int set_blocking(int fd, int should_block);
int set_interface_attribs(int fd, speed_t speed, int parity);
speed_t decodeBaudRate(unsigned int baudRate);
uint32_t crc32(uint32_t crc, const void *buf, size_t size);
uint8_t get_msp_message(
		msp_parse_state_t *state, 
		uint8_t readByte, 
		uint8_t *mspDataBuf, 
		uint8_t *msgType);

#endif // _TELEWII_H
