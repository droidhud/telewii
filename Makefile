all: telewii

telewii: telewii.c telewii_utils.c telewii.h
	gcc -Os -Wall -std=c99 -lpthread -s -o telewii telewii.c telewii_utils.c

clean:
	rm telewii
