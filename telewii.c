/*
** Copyright 2016 by Joel Fuster <j@fuster.org>
** 
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License version 3, as 
** published by the Free Software Foundation.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
** 02110-1301  USA
*/

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include <termios.h>            /* for serial */
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <getopt.h>
#include <errno.h>
#include <signal.h>

#include "telewii.h"
#include "msp_msg_types.h"

#define REQ_DELAY_US            (1000)
#define MAX_ATT_POLL_FREQ       (200U)
#define MIN_ATT_POLL_FREQ       (1U)
#define DEFAULT_ATT_POLL_FREQ   (60U)
#define DEFAULT_OTHER_POLL_DIV  (12U)
#define MAX_BAUD_RATE           (921600U)
#define MIN_BAUD_RATE           (300U)
#define DEFAULT_BAUD_RATE       (115200)
#define DEFAULT_TELEM_PORT      (1235)
#define MAX_POLL_DIV            (MAX_ATT_POLL_FREQ * 10U)
#define MIN_POLL_DIV            (1U)

#define CLIP(x,min,max)         (((x)>(max)?(max):((x)<(min)?(min):(x))))


#pragma pack(1)
typedef struct {
    msp_status_t    mspStatus;
    msp_raw_imu_t   mspRawImu;
    msp_attitude_t  mspAttitude;
    msp_analog_t    mspAnalog;
    msp_altitude_t  mspAltitude;
    msp_sonar_t     mspSonar;
    msp_servo_t     mspServos;
    msp_motor_t     mspMotors;

    uint32_t        mspBadChecksums;
    uint32_t        mspSyncErrors;
    float           cpuUtilization;
} telem_frame_t;
#pragma pack()


typedef struct {
    /* Config */
    char                *dest;
    char                *serialDev;
    char                *destPort;
    unsigned int        attPollFreq;
    unsigned int        otherPollDiv;
    unsigned int        baudRate;
    speed_t             baudRateCode;
    bool                forceIpv4;

    /* State */
    int                 socketDescriptor;
    int                 serialfd;
    int                 timerfd;
    unsigned int        pollCount;
    bool                terminate;
    uint32_t            seqnum;
    telem_frame_t       currentFrame;
} shared_data_t;

static shared_data_t sharedData;

#if 0
static uint8_t reqMsgIds[] = {   
    MSP_RAW_IMU, MSP_ATTITUDE, MSP_ANALOG, MSP_ALTITUDE, MSP_SONAR_ALTITUDE, 
    MSP_SERVO, MSP_MOTOR, MSP_STATUS
};
#else
static uint8_t reqMsgIds[] = {   
    MSP_RAW_IMU, MSP_ANALOG, MSP_ALTITUDE, MSP_STATUS
};

static uint8_t attReqMsgIds[] = { MSP_ATTITUDE };

#endif

static void sig_handler(int signum);
static uint16_t create_msp_msg(uint8_t msgType, void *data, uint8_t dataLen, void *buffer, uint16_t bufferLen);
static uint32_t create_telem_msg(TELEM_MSG_TYPE type, uint32_t seqnum, void *data, uint8_t dataLen, void *buffer, uint32_t bufferLen);
static void update_telem_from_msp(telem_frame_t *frame, uint8_t msgType, uint8_t *data);
static void usage(char *name);
static void poll_thread(void *dataPtr);
static void forward_thread(void *dataPtr);
static void pollMessages(shared_data_t *data, uint8_t *ids, int numIds);

int main(int argc, char *argv[])
{
    pthread_t           pollThread;
    pthread_t           forwardThread;
    pthread_attr_t      threadAttributes;
    extern char         *optarg;
    extern int          optind, opterr, optopt;
    int                 c;
    int                 rc;
    void                *status;

    sharedData.dest         = NULL;
    sharedData.serialDev    = NULL;
    sharedData.destPort     = NULL;
    sharedData.attPollFreq  = DEFAULT_ATT_POLL_FREQ; 
    sharedData.otherPollDiv = DEFAULT_OTHER_POLL_DIV; 
    sharedData.baudRate     = DEFAULT_BAUD_RATE;
    sharedData.forceIpv4    = false;
    sharedData.terminate    = false;

    opterr = 0;
    while ((c = getopt(argc, argv, "4d:s:p:f:o:")) != -1)
    {
        switch (c) {
            case 'd':
                sharedData.dest = optarg;
                break;
            case 's':
                sharedData.serialDev = optarg;
                break;
            case 'p':
                sharedData.destPort = optarg;
                break;
            case 'f':
                sharedData.attPollFreq = atoi(optarg);
                sharedData.attPollFreq = CLIP(sharedData.attPollFreq, MIN_ATT_POLL_FREQ, MAX_ATT_POLL_FREQ);
                break;
            case 'o':
                sharedData.otherPollDiv = atoi(optarg);
                sharedData.otherPollDiv = CLIP(sharedData.otherPollDiv, MIN_POLL_DIV, MAX_POLL_DIV);
                break;
            case 'b':
                sharedData.baudRate = atoi(optarg);
                sharedData.baudRate = CLIP(sharedData.baudRate, MIN_BAUD_RATE, MAX_BAUD_RATE);
                break;
            case 'h':
                usage(argv[0]);
                exit(0);
                break;
            case '?':
                usage(argv[0]);
                exit(-1);
                break;
        }
    }

    // Were all required arguments set?
    if (NULL == sharedData.dest || NULL == sharedData.serialDev) 
    {
        usage(argv[0]);
        exit(-1);
    }

    // Parse arguments
    if (NULL == sharedData.destPort) 
    {
        if (asprintf(&sharedData.destPort, "%d", DEFAULT_TELEM_PORT) == -1) 
        {
            fprintf(stderr, "ENOMEM?\n");
            exit(-1);
        }
    }

    sharedData.baudRateCode = decodeBaudRate(sharedData.baudRate);
    if (B0 == sharedData.baudRateCode)
    {
        fprintf(stderr, "Invalid baud rate: %u\n", sharedData.baudRate);
        exit(-1);
    }

    /* Make sure we can configure and open serial device */
    sharedData.serialfd = open(sharedData.serialDev, O_RDWR | O_NOCTTY | O_SYNC);
    if (sharedData.serialfd < 0)
    {
        fprintf(stderr, "Failed to open serial device: %s\n", sharedData.serialDev);
        exit(-1);
    }

    rc = set_interface_attribs(sharedData.serialfd, sharedData.baudRateCode, 0);
    if (rc)
    {
        fprintf(stderr, "Failed to set serial port attributes\n");
        exit(-1);
    }

    rc = set_blocking(sharedData.serialfd, 1);
    if (rc)
    {
        fprintf(stderr, "Failed to set blocking state for serial port\n");
        exit(-1);
    }

    /* Make sure we can open socket */
    sharedData.socketDescriptor = open_socket(sharedData.dest, sharedData.destPort, sharedData.forceIpv4);
    
    if (SIG_ERR == signal(SIGINT, sig_handler))
    {
        fprintf(stderr, "Could not register to catch SIGINT\n");
        exit(-1);
    }
   
    printf("Opened serial device %s at baudrate of %u\n", sharedData.serialDev, sharedData.baudRate);
    printf("Polling for attitude telemetry at %u Hz\n", sharedData.attPollFreq);
    printf("Polling for other telemetry at %u/%u Hz\n", sharedData.attPollFreq, sharedData.otherPollDiv);
    printf("Sending UDP packets to %s:%s\n", sharedData.dest, sharedData.destPort);

    pthread_attr_init(&threadAttributes);
    pthread_attr_setdetachstate(&threadAttributes, PTHREAD_CREATE_JOINABLE);

    /* Spawn thread to poll for telemetry */
    rc = pthread_create(&pollThread, &threadAttributes, (void *)poll_thread, (void *) &sharedData);
    if (rc)
    {
        fprintf(stderr, "Failed to create poll thread: %d\n", rc);
        exit(-1);
    }

    /* Spawn thread to read serial packets and forward over UDP */
    rc = pthread_create(&forwardThread, &threadAttributes, (void *)forward_thread, (void *) &sharedData);
    if (rc)
    {
        fprintf(stderr, "Failed to create forwarding thread: %d\n", rc);
        exit(-1);
    }

    pthread_attr_destroy(&threadAttributes);

    /* Wait for threads to terminate */
    pthread_join(pollThread, &status);
    pthread_join(forwardThread, &status);

    close(sharedData.serialfd);
    close(sharedData.socketDescriptor);
    printf("Exiting...\n");
    exit(0);
}

static void sig_handler(int signum)
{
    if (SIGINT == signum)
    {
        printf("Received SIGINT\n");
        sharedData.terminate = true;
    }
}

/* Thread to poll for telemetry packets */
static void poll_thread(void *dataPtr)
{
    shared_data_t      *data = (shared_data_t *)dataPtr;
    struct itimerspec   interval;
    int                 rc;
    uint64_t            numExpirations;

    interval.it_interval.tv_sec = (time_t)0;
    interval.it_interval.tv_nsec = 1000000000L / data->attPollFreq;
    interval.it_value.tv_sec = (time_t)0;
    interval.it_value.tv_nsec = interval.it_interval.tv_nsec;

    /* Set up timer */
    data->timerfd = timerfd_create(CLOCK_MONOTONIC, 0);
    if (data->timerfd < 0)
    {
        fprintf(stderr, "Error creating timer: %d\n", errno);
        exit(-1);
    }

    rc = timerfd_settime(data->timerfd, 0, &interval, NULL);
    if (rc < 0)
    {
        fprintf(stderr, "Error setting timer: %d\n", errno);
        exit(-1);
    }

    /* Loop until termination requested */
    while (!data->terminate)
    {
        /* Wait for timer expiration */
        read(data->timerfd, (void *)&numExpirations, sizeof(numExpirations));

        /* Send attitude poll requests first */
        pollMessages(data, attReqMsgIds, sizeof(attReqMsgIds)/sizeof(attReqMsgIds[0]));

        /* Poll for other messages at a lower rate */
        if (0 == data->pollCount)
        {
            pollMessages(data, reqMsgIds, sizeof(reqMsgIds)/sizeof(reqMsgIds[0]));
        }
        data->pollCount++;
        data->pollCount %= data->otherPollDiv;
    }

    close(data->timerfd);
}

/* Thread to read serial device and forward as UDP packets */
static void forward_thread(void *dataPtr) 
{
    uint8_t             readByte;
    uint32_t            telemMsgLen;
    ssize_t             numBytes;
    shared_data_t       *data = (shared_data_t *)dataPtr;
    msp_parse_state_t   parseState;

    uint8_t             telemMsgBuf[1024];
    uint8_t             mspDataBuf[256];
    uint8_t             msgType;
    uint8_t             dataLen;
    bool                gotLastMessage = false;


    /* Loop until termination requested */
    while (!data->terminate)
    {
        numBytes = read(data->serialfd, (void *)&readByte, 1);

        if (numBytes != 1) continue;

        dataLen = get_msp_message(&parseState, readByte, mspDataBuf, &msgType);

        if (dataLen > 0)
        {
            //fprintf(stderr, "Got msp message type %d\n", msgType);
            update_telem_from_msp(&data->currentFrame, msgType, mspDataBuf);

            if (msgType == attReqMsgIds[sizeof(attReqMsgIds)/sizeof(attReqMsgIds[0])-1])
            {
                data->currentFrame.mspBadChecksums = parseState.mspBadChecksums;
                data->currentFrame.mspSyncErrors = parseState.mspSyncErrors;
                gotLastMessage = true;
            }
        }

        if (true == gotLastMessage)
        {
            //fprintf(stderr, "Got last msp message\n");
            // Create and send UDP packet
            telemMsgLen = create_telem_msg(
                    TELEM_MSG_FRAME, 
                    data->seqnum, 
                    &data->currentFrame, 
                    sizeof(data->currentFrame), 
                    telemMsgBuf, 
                    sizeof(telemMsgBuf));

            if (0 == telemMsgBuf)
            {
                fprintf(stderr, "Could not create telemetry message\n");
                exit(-1);
            } else {
                data->seqnum++;
                if (write(data->socketDescriptor, telemMsgBuf, (size_t)telemMsgLen) < 0) {
                    // FIXME This seems to happen if the app stops listening???
                    //fprintf(stderr, "Could not send data\n");
                }
            }
            gotLastMessage = false;
        }

    } // Terminate?

}

/* Buffer must be at least 16+dataLen bytes in length */
static uint32_t create_telem_msg(TELEM_MSG_TYPE type, uint32_t seqnum, void *data, uint8_t dataLen, void *buffer, uint32_t bufferLen)
{
    telem_header_t *pHeader = (telem_header_t *)buffer;

    if (bufferLen < (dataLen + 16))
    {
        return 0;
    }

    pHeader->sync = htonl(TELEM_SYNC_WORD);
    pHeader->seqnum = htonl(seqnum);
    pHeader->type = htons((uint16_t)type);
    pHeader->data_len = htons((uint16_t)dataLen);
    memcpy(pHeader->data, data, dataLen);
    pHeader->crc = crc32(0UL, &pHeader->seqnum, (dataLen + 8));
    pHeader->crc = htonl(pHeader->crc);

    return dataLen + 16;
}

/* Buffer must be at least 6+dataLen bytes in length */
static uint16_t create_msp_msg(uint8_t msgType, void *data, uint8_t dataLen, void *buffer, uint16_t bufferLen)
{
    uint8_t *bufptr = (uint8_t *)buffer;
    uint8_t *dataptr = (uint8_t *)data;
    uint8_t checksum = 0;

    if (bufferLen < (dataLen + 6))
    {
        return 0;
    }

    bufptr[0] = '$';
    bufptr[1] = 'M';
    bufptr[2] = '<';
    bufptr[3] = dataLen; checksum ^= bufptr[3];
    bufptr[4] = msgType; checksum ^= bufptr[4];

    for (int i = 0; i < dataLen; i++)
    {
        bufptr[i + 5] = dataptr[i];
        checksum ^= bufptr[i + 5];
    }
    bufptr[5 + dataLen] = checksum;

    return dataLen + 6;
}

static void usage(char *name)
{
    fprintf(stdout,
            "USAGE: %s -s <serial dev> -d <dest> [-p <port> -f <attitude freq> -o <other data divisor> -b <baudrate>]\n", name);
}

static void update_telem_from_msp(telem_frame_t *frame, uint8_t msgType, uint8_t *data)
{

    switch (msgType)
    {
        case MSP_STATUS:
            memcpy(&frame->mspStatus, data, sizeof(frame->mspStatus));
            break;

        case MSP_RAW_IMU:
            memcpy(&frame->mspRawImu, data, sizeof(frame->mspRawImu));
            break;

        case MSP_ATTITUDE:
            memcpy(&frame->mspAttitude, data, sizeof(frame->mspAttitude));
            break;

        case MSP_ANALOG:
            memcpy(&frame->mspAnalog, data, sizeof(frame->mspAnalog));
            break;

        case MSP_ALTITUDE:
            memcpy(&frame->mspAltitude, data, sizeof(frame->mspAltitude));
            break;

        case MSP_SONAR_ALTITUDE:
            memcpy(&frame->mspSonar, data, sizeof(frame->mspSonar));
            break;

        case MSP_SERVO:
            memcpy(&frame->mspServos, data, sizeof(frame->mspServos));
            break;

        case MSP_MOTOR:
            memcpy(&frame->mspMotors, data, sizeof(frame->mspMotors));
            break;

        default:
            break;
    }

}

static void pollMessages(shared_data_t *data, uint8_t *ids, int numIds)
{
    int                 reqIndex = 0;
    uint8_t             reqMsg[MSP_HEADER_LEN];
    ssize_t             numBytes;
    int                 rc;

    for (reqIndex = 0; reqIndex < numIds; reqIndex++)
    {
        rc = (int)create_msp_msg(ids[reqIndex], NULL, 0, reqMsg, sizeof(reqMsg));
        if (rc > 0)
        {
            numBytes = write(data->serialfd, (const void *)reqMsg, sizeof(reqMsg));
            if (numBytes < sizeof(reqMsg))
            {
                fprintf(stderr, "Error sending poll request: %d\n", errno);
            }
        } else {
            fprintf(stderr, "Error creating MSP message\n");
        }
        usleep((useconds_t)REQ_DELAY_US);
    }
}
